import { shallowMount } from "@vue/test-utils";

import ResizeObserver from "@/components/ResizeObserver.vue";
import ResizeObserverPolyFill from "resize-observer-polyfill";
import { debounceByAnimationFrame } from "../../src/utils";

jest.mock("resize-observer-polyfill");
jest.mock("../../src/utils", () => ({
  debounceByAnimationFrame: jest.fn().mockImplementation(cb => cb)
}));

describe("ResizeOberver.vue", () => {
  let wrapper;

  // helpers
  const getResizeObserverInstance = () =>
    ResizeObserverPolyFill.mock.instances[0];
  const getResizeObserverHandler = () =>
    ResizeObserverPolyFill.mock.calls[0][0];

  const mountComponent = (options = {}) => {
    wrapper = shallowMount(ResizeObserver, options);
  };

  beforeEach(() => {
    ResizeObserverPolyFill.mockClear();
    debounceByAnimationFrame.mockClear();
    mountComponent();
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("wraps with a div", () => {
    expect(wrapper.find("div").exists()).toBe(true);
  });

  it("has a default slot", () => {
    mountComponent({
      slots: {
        default: `<span class="default-slot-content"></span>`
      }
    });
    expect(wrapper.find(".default-slot-content").exists()).toBe(true);
  });

  it("instanciates a new ResizeObserver when mounted", () => {
    expect(ResizeObserverPolyFill).toHaveBeenCalledTimes(1);
    expect(ResizeObserverPolyFill.mock.instances).toHaveLength(1);
  });

  it("passes a handler to ResizeObserver", () => {
    const handler = getResizeObserverHandler();
    expect(typeof handler).toBe("function");
  });

  it("emits 'resize' event when the handler gets called", () => {
    const handler = getResizeObserverHandler();
    handler([]);
    expect(wrapper.emitted().resize).toHaveLength(1);
    expect(getResizeObserverInstance().observe).toHaveBeenCalledTimes(1);
  });

  it("passes the first event-data entry to the emitter", () => {
    const handler = getResizeObserverHandler();
    const mockResizeEventData = [{}, {}, {}];
    handler(mockResizeEventData);
    expect(wrapper.emitted().resize[0][0]).toBe(mockResizeEventData[0]);
  });

  it("uses debounceByAnimationFrame for the handler", () => {
    const handler = getResizeObserverHandler();
    handler([]);
    expect(debounceByAnimationFrame.mock.calls).toHaveLength(1);
  });

  it("disconnects from the ResizeObserver instance when destroyed", () => {
    wrapper.destroy();
    expect(getResizeObserverInstance().disconnect).toHaveBeenCalledTimes(1);
  });
});
